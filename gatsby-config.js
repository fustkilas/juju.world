require("dotenv").config()
module.exports = {
  siteMetadata: {
    title: `Juju by Brud`,
    description: `Juju`,
    author: `Brud`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-lodash`,
    `gatsby-plugin-styled-components`,
    `gatsby-transformer-sharp`,
    `gatsby-transformer-remark`,

    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        defaultQuality: 100,
      },
    },
    // {
    //   resolve: `gatsby-plugin-typography`,
    //   options: {
    //     pathToConfigModule: `src/utils/typography`,
    //   },
    // },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/src/pages/`,
      },
    },
    {
      resolve: `gatsby-source-airtable`,
      options: {
        apiKey: process.env.GATSBY_AIRTABLE_APIKEY,
        tables: [
          {
            baseId: process.env.GATSBY_AIRTABLE_BASEID,
            tableName: `People`,
            tableView: `JujuView`,
            tableLinks: [`JujuShows`],
            mapping: { ArtistPic: `fileNode`, Bio: `text/markdown` },
          },
          {
            baseId: process.env.GATSBY_AIRTABLE_BASEID,
            tableName: `Juju`,
            tableView: `Web`,
            tableLinks: [`Jujus`],
            mapping: { JujuImg: `fileNode`, JujuDesc: `text/markdown` },
          },
        ],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Juju`,
        short_name: `Juju`,
        start_url: `/`,
        display: `standalone`,
        icon: `src/images/icon.png`,
        include_favicon: true,
        background_color: `#ff00ff`,
        theme_color: `#ff00ff`,
      },
    },
  ],
}
