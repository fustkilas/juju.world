const path = require(`path`)
global.Promise = require("bluebird")

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions
  const artistTemplate = path.resolve(`src/templates/artist.js`)
  const emanationTemplate = path.resolve(`src/templates/emanation.js`)

  return graphql(`
    {
      artist: allAirtable(filter: { table: { eq: "People" } }) {
        edges {
          node {
            data {
              Slug
            }
          }
        }
      }

      emanation: allAirtable(filter: { table: { eq: "Juju" } }) {
        edges {
          node {
            data {
              JujuTitle
            }
          }
        }
      }
    }
  `).then(result => {
    if (result.errors) {
      throw result.errors
    }

    result.data.artist.edges.forEach(edge => {
      createPage({
        path: `/module/${edge.node.data.Slug}`,
        component: artistTemplate,
        context: {
          Slug: edge.node.data.Slug,
        },
      })
    })
    result.data.emanation.edges.forEach(edge => {
      createPage({
        path: `/emanation/${edge.node.data.JujuTitle}`,
        component: emanationTemplate,
        context: {
          JujuTitle: edge.node.data.JujuTitle,
        },
      })
    })
  })
}
