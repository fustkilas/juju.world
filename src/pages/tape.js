import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import ReactPlayer from "react-player"
import Logotype from "../components/logotype"
import Links from "../components/links"

const Tape = () => (
  <Layout>
    <SEO title="Juju" keywords={[`Brud`, `Juju`, `Rib`]} />
    <Logotype />
    <section>
      <h1>Ping-Pong</h1>
    </section>
    <section>
      <ReactPlayer
        title="Radio Juju"
        width="100%"
        controls="true"
        loop="true"
        playing="true"
        url="https://soundcloud.com/u33o/sets/radio-juju"
      />
    </section>
    <Links />
    <Logotype />
  </Layout>
)

export default Tape
