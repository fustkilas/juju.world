import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import ReactPlayer from "react-player"
import Logotype from "../components/logotype"
import Links from "../components/links"

const TV = () => (
  <Layout>
    <SEO title="Juju" keywords={[`Brud`, `Juju`, `Rib`]} />
    <Logotype />
    <section>
      <ReactPlayer
        title="Juju TV"
        width="100%"
        controls="true"
        loop="true"
        playing="true"
        url="https://www.youtube.com/watch?v=c8B7L3lgfdo&list=PL7F7-9TLzjhgqbccyZtOR-CkPQ0HulT4m"
      />
    </section>
    <Links />
    <Logotype />
  </Layout>
)

export default TV
