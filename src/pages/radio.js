import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import ReactPlayer from "react-player"
import Logotype from "../components/logotype"
import Links from "../components/links"
import Stream from "../components/stream"

const Radio = () => (
  <Layout>
    <SEO title="Juju" keywords={[`Brud`, `Juju`, `Rib`]} />
    <Logotype />
    <section>
      <Stream />
      <ReactPlayer
        title="Radio Juju"
        width="100%"
        controls="true"
        loop="true"
        playing="true"
        url="https://soundcloud.com/ummoguru/sets/radio-juju"
      />
    </section>
    <Links />
    <Logotype />
  </Layout>
)

export default Radio
