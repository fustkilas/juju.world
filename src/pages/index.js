import React from "react"
import Layout from "../components/layout"
import Logotype from "../components/logotype"
import SEO from "../components/seo"
import { graphql } from "gatsby"
import Modules from "../components/modules"
import Dates from "../components/dates"
import Radio from "../components/radio"
import TV from "../components/tv"
import Info from "../components/info"
import Intro from "../components/intro"
import Links from "../components/links"
import Stream from "../components/stream"
import Slogan from "../components/slogan"

const IndexPage = ({ data }) => (
  <Layout>
    <SEO title="Juju" keywords={[`Brud`, `Juju`, `Rib`]} />
    <Logotype />
    <Links />
    <Slogan />
    <Dates data={data.dates} />
    <Modules data={data.modules} />
    <Radio />
    <TV />
    <Info />
    <Links />
    <Logotype />
  </Layout>
)

export default IndexPage

export const query = graphql`
  query {
    dates: allAirtable(
      filter: { table: { eq: "Juju" } }
      sort: { fields: [data___JujuDate], order: DESC }
    ) {
      edges {
        node {
          data {
            JujuTitle
            JujuDate(formatString: "MMM DD")
            JujuYoutube
            JujuVenue
            JujuDesc {
              childMarkdownRemark {
                html
              }
            }
            Jujus {
              data {
                Full_Name
                First_Name
              }
            }
          }
        }
      }
    }
    modules: allAirtable(
      filter: { table: { eq: "People" } }
      sort: { fields: [data___First_Name], order: DESC }
    ) {
      edges {
        node {
          data {
            First_Name
            Full_Name
            PicURL
            Slug
            Bio {
              childMarkdownRemark {
                html
              }
            }
          }
        }
      }
    }
  }
`
