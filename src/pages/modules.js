import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Logotype from "../components/logotype"
import Links from "../components/links"
import Modules from "../components/modules"
import { graphql } from "gatsby"

const ModulesPage = ({ data }) => (
  <Layout>
    <SEO title="Juju" keywords={[`Brud`, `Juju`]} />
    <Logotype />
    <section>
      <Modules data={data.modules} />
    </section>
    <Links />
    <Logotype />
  </Layout>
)

export default ModulesPage

export const query = graphql`
  query {
    modules: allAirtable(
      filter: { table: { eq: "People" } }
      sort: { fields: [data___First_Name], order: ASC }
    ) {
      edges {
        node {
          data {
            First_Name
            Full_Name
            Slug
            PicURL
            Bio {
              childMarkdownRemark {
                html
              }
            }
          }
        }
      }
    }
  }
`
