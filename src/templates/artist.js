import React from "react"
import { graphql } from "gatsby"
import SEO from "../components/seo"
import Layout from "../components/layout"
import Logotype from "../components/logotype"
import Links from "../components/links"
import { Text } from "rebass"
import Imgix from "react-imgix"


const Artist = ({ data }) => (
  <Layout>
    <SEO title={data.artist.data.Full_Name} keywords={[`brud`, `juju`]} />
    <Logotype />
    <section class="bio">
      <div class="headshot">
        <Imgix
          src={`${data.artist.data.PicURL}`}

        />
      </div>
      <Text
        dangerouslySetInnerHTML={{
          __html: data.artist.data.Bio.childMarkdownRemark.html,
        }}
      />
    </section>
    <Links />
    <Logotype />
  </Layout>
)

export default Artist

export const query = graphql`
  query($Slug: String) {
    artist: airtable(data: { Slug: { eq: $Slug } }) {
      data {
        Full_Name
        First_Name
        Slug
        PicURL
        Bio {
          childMarkdownRemark {
            html
          }
        }
      }
    }
  }
`
