import React from "react"
import { graphql } from "gatsby"
import Imgix from "react-imgix"
import _ from "lodash"
import Layout from "../components/layout"
import Logotype from "../components/logotype"
import SEO from "../components/seo"
import ReactPlayer from "react-player"
import { Text } from "rebass"
import { Link } from "gatsby"
import Links from "../components/links"

const Emanation = ({ data }) => (
  <Layout>
    <SEO title={data.emanation.data.JujuTitle} keywords={[`brud`, `juju`]} />
    <Logotype />
    <section>
        <div class="details">
          <h2>{data.emanation.data.JujuTitle}</h2>
          <h3>{data.emanation.data.JujuDate}</h3>
          <h4>{data.emanation.data.JujuVenue}</h4>
      </div>
      <p>
        <em>featuring</em>
      </p>
      <ul class="title">
        {_.reverse(data.emanation.data.Jujus).map((edge, i) => (
          <li>
            <Link to={`/module/${edge.data.Slug}`}>
                          <Imgix
                className="artistPic"
                src={`${edge.data.PicURL}? fit=facearea&crop=faces&w=30&h=30&q=100`}
              />

              {edge.data.Full_Name}
            </Link>
          </li>
        ))}
      </ul>
      <section>
        {!!data.emanation.data.JujuYoutube && (
          <ReactPlayer
            width="100%"
            controls="true"
            loop="true"
            url={`${data.emanation.data.JujuYoutube}`}
          />
        )}
      </section>
      <section class="emanationDescription">
        {!!data.emanation.data.JujuDesc && (
          <Text
            dangerouslySetInnerHTML={{
              __html: data.emanation.data.JujuDesc.childMarkdownRemark.html,
            }}
          />
        )}
      </section>
    </section>
    <Links />

    <Logotype />
  </Layout>
)

export default Emanation

export const query = graphql`
  query($JujuTitle: String) {
    emanation: airtable(data: { JujuTitle: { eq: $JujuTitle } }) {
      data {
        JujuTitle
        JujuDate(formatString: "MMMM D YYYY")
        JujuYoutube
        JujuVenue
        JujuDesc {
          childMarkdownRemark {
            html
          }
        }
        Jujus {
          data {
            Full_Name
            First_Name
            PicURL
            Slug
          }
        }
      }
    }
  }
`
