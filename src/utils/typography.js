import Typography from "typography"

const typography = new Typography({
  baseFontSize: "15px",
  baseLineHeight: 1.5,
  scaleRatio: 3,
  bodyFontFamily: ["acumin-pro"],
  headerFontFamily: ["acumin-pro"],
  bodyWeight: 200,
  bodyGray: 80,
  bodyGrayHue: "slate",
})

export default typography
