import styled from "styled-components"
import { Box } from "rebass"

const Section = styled(Box)({
  marginBottom: "2em",
})

export default props => <Section>{props.children}</Section>
