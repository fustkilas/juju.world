import React from "react"
import ReactPlayer from "react-player"

const TV = () => (
  <>
    <section>
      <h2 class="title">Juju TV</h2>

      <ReactPlayer
        title="Juju TV"
        width="100%"
        controls="true"
        loop="true"
        url="https://www.youtube.com/watch?v=c8B7L3lgfdo&list=PL7F7-9TLzjhgqbccyZtOR-CkPQ0HulT4m"
      />
    </section>
  </>
)

export default TV
