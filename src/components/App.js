import React from "react"

const App = props => (
  <>
    <section>
      <p>
        In keeping with Brud's continued excavation of the
        <em> camera obscura</em>, the app has two behaviours: <em>in-camera</em>
        , and <em>ex-camera</em>; <em>camera</em> being Latin for <em>room</em>.
        The app behaves differently <em>inside (in-)</em> vs.{" "}
        <em>outside (ex-)</em>.
        <a href="https://itunes.apple.com/us/app/juju-by-brud/id1456524517?ls=1&mt=8">
          Juju is available at the App Store.
        </a>
      </p>
    </section>
  </>
)
export default App
