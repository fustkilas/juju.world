import React from "react"
import { Box, Text } from "rebass"

const Next = props => (
  <>
    <Box>
      <Box my={[3, 4, 5]}>
        <Text my={[4, 4, 5]} fontSize={[4, 5, 5]}>
          Next Up
        </Text>
        <Text my={[2, 3, 4]} fontSize={[2, 3, 3]}>
          Friday, March 29 <br />
        </Text>
        <Text my={[2, 2, 2]} fontSize={[3, 4, 4]}>
          <a href="/emanation/Moderato">
            Gerriet Krishna Sharma
            <br />
            Gita
          </a>
        </Text>
      </Box>
      <Box my={[4, 4, 5]}>
        <Text my={[2, 2, 2]} fontSize={[2, 3, 3]}>
          Saturday, March 30 <br />
        </Text>
        <Text my={[2, 2, 2]} fontSize={[3, 4, 4]}>
          <a href="/emanation/Moderato">
            Meng Qi <br />
            Live from Beijing
          </a>
        </Text>
      </Box>
    </Box>
  </>
)
export default Next
