import React from "react"
import styled from "styled-components"

const Tile = styled.div`
  display: grid;
  grid-gap: 0em;
  grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
  margin: 0 auto;
  padding: 4em 0;
  grid-auto-flow: dense;
`

export default props => <Tile>{props.children}</Tile>
