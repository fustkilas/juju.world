import React from "react"
import styled from "styled-components"

const Grid = styled.div`
  display: grid;
  max-width: 100%;
  grid-gap: 1em;
  grid-template-columns: repeat(auto-fit, minmax(260px, 1fr));
  align-items: center;
  justify-items: center;
`

export default props => <Grid>{props.children}</Grid>
