import React from "react"
import { Link } from "gatsby"
import Imgix from "react-imgix"

const Modules = ({ data }) => (
  <>
    <section>
        <Link to={`/modules`}>
            <h2 class="title">Modules</h2>
        </Link>

      <ul class="modules">
        {data.edges.map((edge, i) => (
          <li>
            <Link to={`/module/${edge.node.data.Slug}`}>
              <Imgix
                className="artistPic"
                src={`${
                  edge.node.data.PicURL
                }? fit=facearea&crop=faces&w=180&h=180&q=100`}
              />
              <p class="moduleName">
              {edge.node.data.Full_Name}
              </p>
            </Link>
          </li>
        ))}
      </ul>
    </section>
  </>
)

export default Modules
