import React from "react"

const Info = props => (
  <>
    <section class="title">
      {/* <h2>Thank You</h2> */}
      <p>
        <a href="http://ribrib.nl">Rib</a>
        <br />
        Katendrechtse Lagedijk 490B <br />
        3082 GJ Rotterdam <br />
        The Netherlands
      </p>

      <ul>
        <li>
          <a href="https://brud.xyz">Brud</a>
        </li>
        <li>
          <a href="https://bitwig.com">Bitwig Studio</a>
        </li>
        <li>
          <a href="https://www.mondriaanfonds.nl/">Mondriaan Fonds</a>
        </li>
        <li>
          <a href="https://stimuleringsfonds.nl/">Stimulerings Fonds</a>
        </li>
        <li>
          <a href="https://www.rotterdam.nl/">Gemeente Rotterdam</a>
        </li>
      </ul>
    </section>
  </>
)
export default Info
