import React from "react"
import styled from "styled-components"

const Nav = styled.div`
  display: grid;
  max-width: 20%;
  grid-gap: 2em;
  grid-template-rows: repeat(auto-fit, minmax(160px, 1fr));
`

export default props => <Nav>{props.children}</Nav>
