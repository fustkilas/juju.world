import React from "react"
import { Box, Text } from "rebass"
import { Link } from "gatsby"

const InfoBox = props => (
  <>
    <Box
      id={props.id}
      my={[3, 5, 6]}
      css={{
        margin: "0 auto",
        textAlign: "center",
      }}
    >
      <Text px={5} my={[4, 4, 5]} fontSize={[4, 5, 5]}>
        App
      </Text>
      <Text px={5} my={[4, 4, 5]} fontSize={[3, 4, 4]}>
        <Link to="https://itunes.apple.com/us/app/juju-by-brud/id1456524517?ls=1&mt=8">
          Juju is available at the App Store.
        </Link>
      </Text>

      <Text px={5} my={[4, 4, 5]} fontSize={[3, 4, 4]}>
        In keeping with Brud's continued excavation of the{" "}
        <em>camera obscura</em>, the app has two behaviours: <em>in-camera</em>,
        and <em>ex-camera</em>, <em>camera</em> being used here in the Latinate
        sense of <em>room</em>. The app behaves differently based on whether one
        is <em>inside</em> the White Cube, or <em>outside (ex)</em>. The
        blackbox in your hand will act funny in the whitecube.
      </Text>
    </Box>
  </>
)
export default App
