import React from "react"
import styled from "styled-components"
import { Heading } from "rebass"

const He = props => (
  <Heading
    {...props}
    css={{
      fontWeight: "400",
    }}
  />
)

export default He
