import React from "react"
import { Box, Card, Heading, Text } from "rebass"

const Blurb = props => (
  <>
    <Box
      px={3}
      my={[3, 4, 5]}
      fontSize={[3, 3, 5]}
      color="#646464"
      width={[1 / 1.5]}
      css={{
        margin: "0 auto",
      }}
    >
      <p>
        Juju is a Modular Synthesiser. Juju is made of <em>Homo Modulus</em>.
        Jujus are touch sensitive, detect gestures, vocode, and have many Ins
        and Outs. Juju music is <em>Modular Folk</em>.
      </p>
    </Box>
  </>
)
export default Blurb
