import React from "react"
import { Link } from "gatsby"
import { Box, Text } from "rebass"
import styled from "styled-components"

const Meny = () => (
  <>
    <nav>
      <ul>
        <li>
          <a href="/#modules">Modules</a>
        </li>
        <li>
          <a href="/#tv">Teevee</a>
        </li>
        <li>
          <a href="/#dates">Dates</a>
        </li>
        <li>
          <a href="/#radio">Radio</a>
        </li>
        <li>
          <a href="/#tape">Tape</a>
        </li>
        <li>
          <a href="/#info">Info</a>
        </li>
      </ul>
    </nav>
  </>
)

export default Meny
