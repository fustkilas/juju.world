import React from "react"

const Stream = props => (
  <section>

    <iframe class="stream" title="Radio JUJU" frameborder="0" width="280" height="216" src="https://brud.airtime.pro/embed/player?stream=auto"></iframe>

  </section>
)
export default Stream
