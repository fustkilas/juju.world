import React from "react"

const Links = props => (
  <>
    <section class="title links">
      <a href="https://www.youtube.com/channel/UCwfN5FXq1TxAuneaqUs6UNg">
        <span role="img" aria-label="Youtube">
          🤡
        </span>{" "}
      </a>
      <a href="https://soundcloud.com/ummoguru/">
        <span role="img" aria-label="Soundcloud">
          👹{" "}
        </span>
      </a>
      <a href="https://muto.casa/">
        <span role="img" aria-label="Bandcamp">
          👺{" "}
        </span>
      </a>
      <a href="https://itunes.apple.com/us/app/juju-by-brud/id1456524517?ls=1&mt=8">
        <span role="img" aria-label="App Store">
          🧠
        </span>
      </a>
    </section>
  </>
)
export default Links
