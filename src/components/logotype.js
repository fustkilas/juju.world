import React from "react"
import { Link } from "gatsby"

const Logotype = props => (
  <>
    <h1 class="logotype">
      <Link to="/">Juju</Link>
    </h1>
    <h1 class="brud">
      <Link to="/">by Brud</Link>
    </h1>
  </>
)

export default Logotype
