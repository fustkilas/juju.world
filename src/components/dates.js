import React from "react"
import { Link } from "gatsby"

const Dates = ({ data }) => (
  <section class="dates">
    <h2 class="title">Dates</h2>
    <ul>
      {data.edges.map((edge, i) => (
        <li>
          <Link to={`/emanation/${edge.node.data.JujuTitle}`}>
            <h4>{edge.node.data.JujuTitle}</h4>
          </Link>
          <Link to={`/emanation/${edge.node.data.JujuTitle}`}>
          <h4 class="date">{edge.node.data.JujuDate}</h4>
          </Link>

        </li>
      ))}
    </ul>
  </section>
)

export default Dates
