import React from "react"
import ReactPlayer from "react-player"

const Radio = ({ data }) => (
  <>
    <section>
      <h2 class="title">More Juju</h2>

      <ReactPlayer
        title="Radio Juju"
        width="100%"
        controls="false"
        loop="true"
        url="https://soundcloud.com/ummoguru/sets/radio-juju"
        />
    </section>
  </>
)

export default Radio
